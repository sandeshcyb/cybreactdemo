export const graphData = () =>{
    return [
        {
            name : 'Low',
            range : 9,
            monthYear : 'Oct 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 2,
            monthYear : 'Oct 2018',
            year :2018
        },
        {
            name : 'High',
            range : 3,
            monthYear : 'Oct 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 19,
            monthYear : 'Nov 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 3,
            monthYear : 'Nov 2018',
            year :2018
        },
        {
            name : 'High',
            range : 10,
            monthYear : 'Nov 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 10,
            monthYear : 'Dec 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 8,
            monthYear : 'Dec 2018',
            year :2018
        },
        {
            name : 'High',
            range : 3,
            monthYear : 'Dec 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 16,
            monthYear : 'Jan 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 12,
            monthYear : 'Jan 2018',
            year :2018
        },
        {
            name : 'High',
            range : 2,
            monthYear : 'Jan 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 18,
            monthYear : 'Feb 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 15,
            monthYear : 'Feb 2018',
            year :2018
        },
        {
            name : 'High',
            range : 3,
            monthYear : 'Feb 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 20,
            monthYear : 'Mar 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 3,
            monthYear : 'Mar 2018',
            year :2018
        },
        {
            name : 'High',
            range : 5,
            monthYear : 'Apr 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 10,
            monthYear : 'Apr 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 4,
            monthYear : 'Apr 2018',
            year :2018
        },
        {
            name : 'High',
            range : 7,
            monthYear : 'May 2018',
            year :2018
        },
        {
            name : 'Low',
            range : 15,
            monthYear : 'Apr 2018',
            year :2018
        },
        {
            name : 'Medium',
            range : 5,
            monthYear : 'May 2018',
            year :2018
        },
        {
            name : 'High',
            range : 9,
            monthYear : 'May 2018',
            year :2018
        },

    ]
}