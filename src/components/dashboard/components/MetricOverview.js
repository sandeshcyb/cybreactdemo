// ****************************************************************************
// ***************** Component : Metric Overview        ***********************
// ***************** Developer : Vikas Jagdale (06/05/2019)  ******************
// ****************************************************************************


import React,{Component} from 'react';
import {metricApi} from '../../../api/metricApi';

class MetricOverview extends Component{
    state={
        metricArray : [],
    }
    componentDidMount(){
        const date = new Date();
        const currentYear = date.getFullYear();
        const lastYear = date.getFullYear()-1;
        this.setState({
            metricArray : metricApi().filter((data)=> data.lastYear==lastYear && data.thisYear==currentYear),
        });
    }
    render(){
        var metricArray = this.state.metricArray;
        if(this.props.sendYear){
            const year = this.props.sendYear;
            const splitYear = year.split('-');
            const [lastYear,currentYear] = splitYear;
            if(splitYear){
                metricArray = metricApi().filter((data)=> data.lastYear==lastYear && data.thisYear==currentYear);
            }
        }
        return(
            <div className="col-lg-6 col-md-6 col-sm-6">
                <div className="col-lg-12 metric-alert-wrap">
                    <div className="col-lg-12 metric-alert-Tit">Metric Overview</div>
                    <table className="table table-borderless pros-borderless-tab">
                        <thead>
                            <tr>
                            <th scope="col">Metric</th>
                            <th scope="col">This Year</th>
                            <th scope="col">Last Year</th>
                            </tr>
                        </thead>
                        {metricArray.length>0 ?
                            <tbody>
                                {metricArray.map((values,index)=>{
                                   return <tr key={index}>
                                        <td>
                                            {values.thisYearMetric>values.lastYearMetric 
                                                ? <i className="fa fa-arrow-up pros-arrow-up"></i>:<i className="fa fa-arrow-down pros-arrow-down"></i>}
                                            {values.metricName}</td>
                                        <td>{values.thisYearMetric.toLocaleString('en-IN')}</td>
                                        <td>{values.lastYearMetric.toLocaleString('en-IN')}</td>
                                    </tr>
                                })}
                            </tbody>
                            :
                            <tbody>
                                <tr>
                                    <td colSpan="3">Nothing to display...</td>
                                </tr>
                            </tbody>
                            }
                            
                        
                        </table>
                </div>
            </div>
        );
    }
}
export default MetricOverview;